
#if ESP8266
#include <ESP8266HTTPClient.h>
#else
#include <HTTPClient.h>
#endif
#include <ArduinoJson.h>

#define LOG_DOMAIN LOG_DOMOTICZ
#include "debug.h"
#include "config.h"
#include "domoticz.h"

String domoticzUrl(DEFAULT_DOMOTICZ_URL);

// get the device IDX by name
// this code can be used if the number of devices is low
// in my case (25 devices) ESP8266 has not enough memory to load the JSON results

int getDeviceId(const char *filter, const char *name)
{
  int idx = 0;
  WiFiClient client;
  HTTPClient http;
  int httpCode;
  // https://arduinojson.org/v5/assistant/
  const size_t bufferSize = 25*JSON_ARRAY_SIZE(1) + JSON_ARRAY_SIZE(25) + JSON_OBJECT_SIZE(7) + 25*JSON_OBJECT_SIZE(45);
  
  log_printf("search for %s\n", name);
  log_printf("buffer size %lu\n", bufferSize);
  String request = domoticzUrl;
  request += "/json.htm?type=devices&filter=";
  request += filter;
  request += "&used=true&order=Name";
  http.begin(client, request);
  httpCode = http.GET();
  log_printf("%s %d\n", request.c_str(), httpCode);
  if (httpCode > 0) {
    DynamicJsonBuffer jsonBuffer(bufferSize);
    JsonObject& root = jsonBuffer.parseObject(http.getString());
    for (int i = 0 ; i < 20 ; i++) {
    String s = root["result"][i]["Name"];
//      log_println(s);
      if (s == "") {
        log_printf("%s not found\n", name);
        break;
      }
      if (s == name) {
        idx = root["result"][i]["idx"];
        break;
      }
    }
  }
  http.end();
  log_printf("IDX=%d\n", idx);
  return idx;
}
