
#if ESP8266
#include <ESP8266HTTPClient.h>
#else
#include <HTTPClient.h>
#endif
#include <ArduinoJson.h>

#define LOG_DOMAIN LOG_INFOS
#include "debug.h"
#include "config.h"
#include "domoticz.h"
#include "utf8.h"
#include "info.h"

unsigned long infoPeriod = DEFAULT_INFO_PERIOD;

struct device deviceList[NDEVICES] = 
{
  "device 1", 1, false, false, false,
  "device 2", 2, false, false, false,
  "device 3", 3, false, false, false,
  "device 4", 4, false, false, false,
};

void getSensorInfo(int idx, float *temp, unsigned *hum, unsigned *batt)
{
  WiFiClient client;
  String request;
  HTTPClient http;
  int httpCode;
  // https://arduinojson.org/v5/assistant/
  const size_t bufferSize = 2 * JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(7) + JSON_OBJECT_SIZE(50);

  log_printf("get infos for %d\n", idx);
  // INFORMATION LINE 1
  if (idx == 0) {
    *temp = 0;
  }
  else {
    request = domoticzUrl;
    request += "/json.htm?type=devices&rid=";
    request += idx;
    http.begin(client, request);
    httpCode = http.GET();
    log_printf("%s %d\n", request.c_str(), httpCode);
    if (httpCode > 0) {
      DynamicJsonBuffer jsonBuffer(bufferSize);
      JsonObject& root = jsonBuffer.parseObject(http.getString());
      if (temp) {
        *temp = root["result"][0]["Temp"];
      }
      if (hum) {
        *hum = root["result"][0]["Humidity"];
      }
      if (batt) {
        *batt = root["result"][0]["BatteryLevel"];
      }
    }
  }
  http.end();
}
