
#include <Arduino.h>
#include <EEPROM.h>

#define LOG_DOMAIN LOG_CONFIG
#include "debug.h"
#include "domoticz.h"
#include "ephemeris.h"
#include "weather.h"
#include "temp.h"
#include "info.h"
#include "config.h"

int saveConfiguration(void)
{
  struct eepromConfig eeprom;

  log_print("domoticz: "); log_println(domoticzUrl);
  log_print("ephemeris: "); log_println(ephemerisUrl);
  log_print("ntp: "); log_println(ntpUrl);
  log_print("weather city: "); log_println(weatherCity);
  log_print("weather key: "); log_println(weatherKey);
  log_print("weather period: "); log_println(weatherPeriod);
  log_print("weather API usage: "); log_println(weatherApiUsage);
  log_print("weather server error: "); log_println(weatherServerError);
  log_print("weather month: "); log_println(currentMonth);
  log_print("temp period: "); log_println(tempPeriod);
  log_print("info period: "); log_println(infoPeriod);
  strncpy(eeprom.domoticzUrl, domoticzUrl.c_str(), sizeof(eeprom.domoticzUrl));
  strncpy(eeprom.ephemerisUrl, ephemerisUrl.c_str(), sizeof(eeprom.ephemerisUrl));
  strncpy(eeprom.ntpUrl, ntpUrl.c_str(), sizeof(eeprom.ntpUrl));
  strncpy(eeprom.weatherCity, weatherCity.c_str(), sizeof(eeprom.weatherCity));
  strncpy(eeprom.weatherKey, weatherKey.c_str(), sizeof(eeprom.weatherKey));
  eeprom.weatherPeriod = weatherPeriod;
  eeprom.weatherApiUsage = weatherApiUsage;
  strncpy(eeprom.weatherServerError, weatherServerError, sizeof(eeprom.weatherServerError));
  eeprom.currentMonth = currentMonth;
  eeprom.tempPeriod = tempPeriod;
  eeprom.infoPeriod = infoPeriod;
  for (int i = 0 ; i < NDEVICES ; i++) {
    eeprom.deviceList[i].idx = deviceList[i].idx;
    strcpy(eeprom.deviceList[i].prompt, deviceList[i].prompt);
    eeprom.deviceList[i].temp = deviceList[i].temp;
    eeprom.deviceList[i].hum = deviceList[i].hum;
    eeprom.deviceList[i].batt = deviceList[i].batt;
  }
  eeprom.magic = MAGIC;
  EEPROM.put(0, eeprom);
  if (EEPROM.commit()) {
    log_println("EEPROM successfully committed");
    return true;
  }
  log_println("ERROR! EEPROM commit failed");
  return false;
}

int loadConfiguration(void)
{
  struct eepromConfig eeprom;

  EEPROM.begin(1024);
  EEPROM.get(0, eeprom);
  if (eeprom.magic == MAGIC) {
    log_println("configuration:");
    domoticzUrl = eeprom.domoticzUrl;
    ephemerisUrl = eeprom.ephemerisUrl;
    ntpUrl = eeprom.ntpUrl;
    weatherCity = eeprom.weatherCity;
    weatherKey = eeprom.weatherKey;
    weatherPeriod = eeprom.weatherPeriod;
    weatherApiUsage = eeprom.weatherApiUsage;
    strncpy(weatherServerError, eeprom.weatherServerError, sizeof(weatherServerError));
    currentMonth = eeprom.currentMonth;
    tempPeriod = eeprom.tempPeriod;
    infoPeriod = eeprom.infoPeriod;
    for (int i = 0 ; i < NDEVICES ; i++) {
      deviceList[i].idx = eeprom.deviceList[i].idx;
      strcpy(deviceList[i].prompt, eeprom.deviceList[i].prompt);
      deviceList[i].temp = eeprom.deviceList[i].temp;
      deviceList[i].hum = eeprom.deviceList[i].hum;
      deviceList[i].batt = eeprom.deviceList[i].batt;
    }
    log_print("domoticz: "); log_println(domoticzUrl);
    log_print("ephemeris: "); log_println(ephemerisUrl);
    log_print("ntp: "); log_println(ntpUrl);
    log_print("weather city: "); log_println(weatherCity);
    log_print("weather key: "); log_println(weatherKey);
    log_print("weather period: "); log_println(weatherPeriod);
    log_print("weather API usage: "); log_println(weatherApiUsage);
    log_print("weather server error: "); log_println(weatherServerError);
    log_print("weather month: "); log_println(currentMonth);
    log_print("temp period: "); log_println(tempPeriod);
    log_print("info period: "); log_println(infoPeriod);
    ntp.ntpServer(ntpUrl.c_str());
  }
  else {
    log_println("no Configuration found");
  }
  return 0;
}
