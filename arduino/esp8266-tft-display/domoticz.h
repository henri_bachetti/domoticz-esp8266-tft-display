
#ifndef _DOMOTICZ_H_
#define _DOMOTICZ_H_

extern String domoticzUrl;

int getDeviceId(const char *filter, const char *name);

#endif
