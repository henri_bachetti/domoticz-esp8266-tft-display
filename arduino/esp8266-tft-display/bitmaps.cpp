
#include <Arduino.h>
#if ESP8266
#include <FS.h>
#else
#include <FS.h>
#include <SPIFFS.h>
#endif

#include "debug.h"
#include "tft.h"

struct weatherCode
{
  int code;
  const char *dayBitmap;
  const char *nightBitmap;
};

weatherCode weatherCodeTable[] =
{
  395, "0012", "0028",
  392, "0016", "0032",
  389, "0024", "0040",
  386, "0016", "0032",
  377, "0021", "0037",
  374, "0013", "0029",
  371, "0012", "0028",
  368, "0011", "0027",
  365, "0013", "0029",
  362, "0013", "0029",
  359, "0018", "0034",
  356, "0010", "0026",
  353, "0009", "0025",
  350, "0021", "0037",
  338, "0020", "0036",
  335, "0012", "0028",
  332, "0020", "0036",
  329, "0020", "0036",
  326, "0011", "0027",
  323, "0011", "0027",
  320, "0019", "0035",
  317, "0021", "0037",
  314, "0021", "0037",
  311, "0021", "0037",
  308, "0018", "0034",
  305, "0010", "0026",
  302, "0018", "0034",
  299, "0010", "0026",
  296, "0017", "0025",
  293, "0017", "0033",
  284, "0021", "0037",
  281, "0021", "0037",
  266, "0017", "0033",
  263, "0009", "0025",
  260, "0007", "0007",
  248, "0007", "0007",
  230, "0020", "0036",
  227, "0019", "0035",
  200, "0016", "0032",
  185, "0021", "0037",
  182, "0021", "0037",
  179, "0013", "0029",
  176, "0009", "0025",
  143, "0006", "0006",
  122, "0004", "0004",
  119, "0003", "0004",
  116, "0002", "0008",
  113, "0001", "0008",
  0
};


const char *getBitmap(int weatherCode, int isDay)
{
  for (int i = 0 ; weatherCodeTable[i].code ; i++) {
    if (weatherCode == weatherCodeTable[i].code) {
      return isDay ? weatherCodeTable[i].dayBitmap : weatherCodeTable[i].nightBitmap;
    }
  }
  return 0;
}

uint16_t raw[4096];

void drawBitmap(int weatherCode, int isDay, int x, int y)
{
  const char *bitmap = getBitmap(weatherCode, isDay);
  Serial.printf("loading %s\n", bitmap);
  if (bitmap) {
    String name = "/";
    name += bitmap;
    name += ".raw";
    File file = SPIFFS.open(name, "r");
    if (!file) {
      Serial.printf("failed to open %s\n", name.c_str());
      return;
    }
    int i = 0;
    while (file.available()) {
      raw[i++] = (uint16_t)file.read() << 8 | file.read();
    }
    file.close();
    Serial.printf("%d bytes\n", i*2);
    tft.drawRGBBitmap(x, y, (uint16_t *)raw, 64, 64);
  }
}
