
#if ESP8266
#include <ESP8266HTTPClient.h>
#else
#include <HTTPClient.h>
#endif
#include <ArduinoJson.h>

#define LOG_DOMAIN LOG_EPHEMERIS
#include "debug.h"
#include "config.h"
#include "domoticz.h"
#include "utf8.h"
#include "ephemeris.h"

String ephemerisUrl(DEFAULT_EPHEMERIS_URL);

void getSaintOfTheDay(char *saint, int len)
{
  WiFiClient client;
  HTTPClient http;
  // EPHEMERIS
  http.begin(client, ephemerisUrl);
  int httpCode = http.GET();
  log_printf("getSaintOfTheDay %s\n", ephemerisUrl.c_str());
  if (httpCode > 0) {
    String html = http.getString();
    String name = html.substring(html.indexOf("<label>") + 7, html.indexOf("</label>"));
    strncpy(saint, name.c_str(), len);
    saint[len-1] = '\0';
    convertUtf8(saint);
    log_printf("getSaintOfTheDay %s\n", saint);
  }
  http.end();
}
