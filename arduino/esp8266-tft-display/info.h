
#ifndef _INFO_H_
#define _INFO_H_

extern unsigned long infoPeriod;

extern struct device deviceList[NDEVICES];

void getSensorInfo(int idx, float *temp, unsigned *hum, unsigned *batt);

#endif
