
#if ESP8266
#include <ESP8266HTTPClient.h>
#else
#include <HTTPClient.h>
#endif
#include <ArduinoJson.h>

#define LOG_DOMAIN LOG_TEMP
#include "debug.h"
#include "config.h"
#include "domoticz.h"
#include "info.h"
#include "temp.h"

#if SENSOR == HDC2080
#include <HDC2080.h>
#elif SENSOR == SHT31D
#include <Adafruit_SHT31.h>
#else
#include <Adafruit_HTU21DF.h>
#endif

#if SENSOR == HDC2080
HDC2080 hdc2080(0x40);
#elif SENSOR == SHT31D
Adafruit_SHT31 sht31 = Adafruit_SHT31();
#else
Adafruit_HTU21DF htu21 = Adafruit_HTU21DF();
#endif

// default value (see tempSensorBegin)
static int sensorIdx = 26;
unsigned long tempPeriod = DEFAULT_TEMP_PERIOD;

void sendTemperatureHumidity(float temperature, float humidity)
{
  WiFiClient client;
  HTTPClient http;

  if (sensorIdx != 0) {
    String request = domoticzUrl;
    request += "/json.htm?type=command&param=udevice&idx=";
    request += sensorIdx;
    request += "&nvalue=0&svalue=";
    request += temperature;
    request += ";";
    request += humidity;
    request += ";0";
    http.begin(client, request);
    int httpCode = http.GET();
    log_printf("%s %d\n", request.c_str(), httpCode);
    if (httpCode > 0) {
      log_printf("%f %f transmitted OK\n", temperature, humidity);
    }
    http.end();
  }
}

void getTemperatureHumidity(float *temperature, float *humidity)
{
#if SENSOR == HDC2080
  *temperature = hdc2080.readTemp();
  *humidity = hdc2080.readHumidity();
#elif SENSOR == SHT31D
  *temperature = sht31.readTemperature();
  *humidity = sht31.readHumidity();
#else
  *temperature = htu21.readTemperature();
  *humidity = htu21.readHumidity();
#endif
  sendTemperatureHumidity(*temperature, *humidity);
}

bool tempSensorBegin(void)
{
#if SENSOR == HDC2080
  hdc2080.begin();
  hdc2080.setMeasurementMode(TEMP_AND_HUMID);
  hdc2080.setRate(ONE_HZ);
  hdc2080.setTempRes(FOURTEEN_BIT);
  hdc2080.setHumidRes(FOURTEEN_BIT);
  hdc2080.triggerMeasurement();
#elif SENSOR == SHT31D
  if (!sht31.begin(0x44)) {
    log_println("Couldn't find SHT31");
    return false;
  }
#else
  if (!htu21.begin()) {
    log_println("Couldn't find sensor!");
    return false;
  }
#endif
  // this code can be used if the number of temperature devices is low (20 devices max)
  sensorIdx = getDeviceId("temp", THIS_SENSOR_NAME);
  return true;
}
