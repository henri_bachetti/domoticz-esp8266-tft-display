
#if ESP8266
#include <ESP8266HTTPClient.h>
#else
#include <HTTPClient.h>
#endif
#include <ArduinoJson.h>

#define LOG_DOMAIN LOG_WEATHER
#include "debug.h"
#include "weather.h"

String weatherCity;
String weatherKey;
unsigned long weatherPeriod = DEFAULT_WEATHER_PERIOD;
unsigned weatherApiUsage;
char weatherServerError[MAX_ERROR_STRING];
int8_t currentMonth;

bool getWeatherInfo(char *info, size_t len, int *weatherCode)
{
  log_println("getting weather");
  strncpy(info, "Unknown", len);
  *weatherCode = 113;

  bool isDay = true;
  WiFiClient client;
  HTTPClient http;
  // http://api.weatherstack.com/current?access_key=KEY&query=CITY
  String request = WEATHER_URL;
  request += "/current?access_key=";
  request += weatherKey;
  request += "&query=";
  request += weatherCity;
  // used during development to avoid increase API usage
//  weatherApiUsage++;
//  strncpy(weatherServerError, "Weather server not called", sizeof(weatherServerError));
//  log_printf("%s\n", request.c_str());
//  return true;

  // METEO
  http.begin(client, request);
  int httpCode = http.GET();
  log_printf("%s %d\n", request.c_str(), httpCode);
  if (httpCode > 0) {
    weatherApiUsage++;
    // https://arduinojson.org/v5/assistant/
    const size_t bufferSize = 2*JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(4) + JSON_OBJECT_SIZE(9) + JSON_OBJECT_SIZE(16) + 650;
    DynamicJsonBuffer jsonBuffer(bufferSize);
    log_println(http.getString());
    JsonObject& root = jsonBuffer.parseObject(http.getString());
    JsonVariant error = root["error"];
    if (error.success()) {
      const char *error = root["error"]["type"];
      strncpy(info, error != 0 ? error : "unknown_error", len);
      strncpy(weatherServerError, error != 0 ? error : "unknown_error", len);
    }
    else if (root.containsKey("current")) {
      const char *data = root["current"]["weather_descriptions"][0];
      strncpy(info, data ? data : "invalid data", len);
      *weatherCode = root["current"]["weather_code"];
      String d = root["current"]["is_day"];
      log_printf("code %d, day : %s\n", *weatherCode, d.c_str());
      isDay = d == "yes" ? true : false;
      weatherServerError[0] = '\0';
    }
  }
  http.end();
  return isDay;
}
