
#ifndef _TEMP_H_
#define _TEMP_H_

#include "config.h"

extern unsigned long tempPeriod;

bool tempSensorBegin(void);
void getTemperatureHumidity(float *temperature, float *humidity);

#endif
