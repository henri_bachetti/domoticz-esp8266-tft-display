
#ifndef _WEATHER_H_
#define _WEATHER_H_

#include "config.h"

extern String weatherCity;
extern String weatherKey;
extern unsigned long weatherPeriod;
extern unsigned weatherApiUsage;
extern char weatherServerError[MAX_ERROR_STRING];
extern int8_t currentMonth;

bool getWeatherInfo(char *info, size_t len, int *weatherCode);

#endif
