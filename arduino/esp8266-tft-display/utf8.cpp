
#include <Arduino.h>

#include "utf8.h"

// convert ° and é for the TFT
void convertUtf8(char *s)
{
  int len = strlen(s);
  for (int i = 0 ; i < len ; i++) {
    if (s[i] == '\xc2' && s[i + 1] == '\xb0') {
      s[i] = '\xf7';
      memcpy(s + i + 1, s + i + 2, len - i - 1);
    }
    if (s[i] == '\xc3' && s[i + 1] == '\xa9') {
      s[i] = 'e';
      memcpy(s + i + 1, s + i + 2, len - i - 1);
    }
  }
}
