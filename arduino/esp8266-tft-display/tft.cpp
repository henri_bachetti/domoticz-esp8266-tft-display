

#include "debug.h"
#include "config.h"
#include "tft.h"

#undef LOGS_ACTIVE

#define TFT_DC 2
#define TFT_CS 15

Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

uint16_t Text::_margin = 10;

Text::Text(GFXfont *font, uint16_t color, uint16_t size, const char *txt)
{
  _font = font;
  _color = color;
  _size = size;
  _prevx = 0;
  _prevy = 0;
  if (!strlen(txt)) {
    txt = " ";
  }
  else {
    strncpy(buf, txt, MAX_TEXT);
  }
  strcpy(_prevbuf, " ");
}

void Text::setMargin(uint16_t margin)
{
  Text::_margin = margin;
}

uint16_t Text::print(uint16_t x, uint16_t y)
{
  int16_t x1, y1;
  uint16_t w, h;

  if (_prevx == 0) {
    _prevx = x;
  }
  if (_prevy == 0) {
    _prevy = y;
  }
  log_print(F("set font "));
  log_println((unsigned int)_font, HEX);
  tft.setFont(_font);
  tft.setTextSize(_size);
  tft.getTextBounds(_prevbuf, 0, 0, &x1, &y1, &w, &h);
  if (strcmp(buf, _prevbuf)) {
    tft.fillRect(_prevx, _prevy, w, h, ILI9341_BLACK);
    tft.setCursor(x, y);
    tft.setTextColor(_color);
    tft.print(buf);
    _prevx = x;
    _prevy = y;
    strcpy(_prevbuf, buf);
  }
  return h;
}

uint16_t Text::printCentered(uint16_t y)
{
  int16_t x = 0;
  int16_t x1, y1;
  uint16_t w, h;

  log_print(F("Text::printCentered \""));
  log_print(buf);
  log_print(F("\" AT "));
  log_print(y);
  tft.setFont(_font);
  tft.setTextSize(_size);
  tft.getTextBounds(buf, 0, 0, &x1, &y1, &w, &h);
  log_print(F(" width="));
  log_println(w);
  x = (TFT_WIDTH - w) / 2;
  return print(x, y);
}

uint16_t Text::printLeft(uint16_t y)
{
  int16_t x = 0;
  int16_t x1, y1;
  uint16_t w, h;

  log_print(F("Text::printLeft \""));
  log_print(buf);
  log_print(F("\" AT "));
  log_print(y);
  tft.setFont(_font);
  tft.setTextSize(_size);
  tft.getTextBounds(buf, 0, 0, &x1, &y1, &w, &h);
  log_print(F(" width="));
  log_println(w);
  x = _margin;
  return print(x, y);
}

uint16_t Text::printRight(uint16_t y)
{
  int16_t x = 0;
  int16_t x1, y1;
  uint16_t w, h;

  log_print(F("Text::printRight \""));
  log_print(buf);
  log_print("\" AT ");
  log_print(y);
  tft.setFont(_font);
  tft.setTextSize(_size);
  tft.getTextBounds(buf, 0, 0, &x1, &y1, &w, &h);
  log_print(" width=");
  log_println(w);
  x = TFT_WIDTH - w - _margin;
  return print(x, y);
}

uint16_t Text::getHeight(void)
{
  int16_t x = 0;
  int16_t x1, y1;
  uint16_t w, h;

  tft.setFont(_font);
  tft.setTextSize(_size);
  tft.getTextBounds(buf, 0, 0, &x1, &y1, &w, &h);
  return h;
}

uint16_t Text::getWidth(void)
{
  int16_t x1, y1;
  uint16_t w, h;

  tft.setFont(_font);
  tft.setTextSize(_size);
  tft.getTextBounds(buf, 0, 0, &x1, &y1, &w, &h);
  return w;
}

StatusText::StatusText(GFXfont *font, uint16_t backColor, uint16_t color, uint16_t size, const char *txt)
  : Text::Text(font, color, size, txt)
{
  _backColor = backColor;
}

uint16_t StatusText::print(uint16_t x, uint16_t y)
{
  int16_t x1, y1;
  uint16_t w, h;

  log_println(F("StatusText::print"));
  tft.setFont(_font);
  tft.setTextSize(_size);
  if (strcmp(buf, _prevbuf)) {
    tft.getTextBounds(buf, 0, 0, &x1, &y1, &w, &h);
    tft.fillRect(0, TFT_HEIGHT - h - 4, TFT_WIDTH, h + 4, _backColor);
    tft.setCursor(x, TFT_HEIGHT - h - 2);
    tft.setTextColor(_color);
    tft.print(buf);
    strcpy(_prevbuf, buf);
  }
  return h;
}
