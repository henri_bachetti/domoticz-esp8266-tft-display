
#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <NTP.h>

#define HDC2080     1
#define SHT31D      2
#define HTU21D      3
//#define SENSOR      HDC2080
//#define SENSOR      SHT31D
#define SENSOR      HTU21D

#define DEFAULT_DOMOTICZ_URL  "http://192.168.1.134:8080"
#define DEFAULT_EPHEMERIS_URL "http://192.168.1.134/ephemeris.php"

// sensor name in DOMOTICZ
#define THIS_SENSOR_NAME        "ESP8266 TFT"

// sensor IDX in DOMOTICZ
#define THIS_SENSOR_IDX         26

// METEO_KEY: see https://weatherstack.com/signup/free
#define WEATHER_URL             "http://api.weatherstack.com"

#define DEFAULT_WEATHER_PERIOD  (2UL * 60 * 60000)  // 2 Hours
#define DEFAULT_TEMP_PERIOD     (30UL * 60000)      // minutes
#define DEFAULT_INFO_PERIOD     (10UL * 60000)      // minutes

#define MAGIC                   0xDEADBEEF
#define MAX_ERROR_STRING        50
#define MAX_WEATHER_KEY         36
#define MAX_WEATHER_CITY        40

#define NDEVICES                4

struct device
{
  char prompt[10];
  unsigned idx;
  bool temp;
  bool hum;
  bool batt;
};

struct eepromConfig
{
  char domoticzUrl[50];
  char ephemerisUrl[50];
  char ntpUrl[50];
  char weatherCity[MAX_WEATHER_CITY];
  char weatherKey[MAX_WEATHER_KEY];
  unsigned long weatherPeriod;
  unsigned weatherApiUsage;
  char weatherServerError[MAX_ERROR_STRING];
  int8_t currentMonth;
  unsigned long tempPeriod;
  unsigned long infoPeriod;
  struct device deviceList[4];
  uint32_t magic;
};

extern NTP ntp;
extern String ntpUrl;

int saveConfiguration(void);
int loadConfiguration(void);

#endif
