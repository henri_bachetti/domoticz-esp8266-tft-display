
#if ESP8266
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPClient.h>
#include <FS.h>
#else
#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <HTTPClient.h>
#include <FS.h>
#include <SPIFFS.h>
#endif
#include <ArduinoJson.h>
#include <NTP.h>

#define ESP_SPI_FREQ 4000000

#define LOG_DOMAIN LOG_MAIN
#include "debug.h"
#include "tft.h"
#include "temp.h"
#include "info.h"
#include "domoticz.h"
#include "ephemeris.h"
#include "weather.h"
#include "bitmaps.h"

#define BUZ_PIN           16

// put your SSID & password here
const char* ssid = "........";
const char* password = "........";

Text infoText1(NULL, ILI9341_ORANGE, 2, "...");
Text infoText2(NULL, ILI9341_ORANGE, 2, "...");
Text infoText3(NULL, ILI9341_ORANGE, 2, "...");
Text infoText4(NULL, ILI9341_ORANGE, 2, "...");
StatusText infoText5(NULL, ILI9341_LIGHTGREY, ILI9341_BLUE, 2, "...");
Text dayText(NULL, ILI9341_CYAN, 2, "");
Text dateText(NULL, ILI9341_CYAN, 1, "");
Text saintText(NULL, ILI9341_WHITE, 1, "");
Text weatherText(NULL, ILI9341_WHITE, 1, "weather");
Text timeText(NULL, ILI9341_YELLOW, 4, "");
Text tempText(NULL, ILI9341_ORANGE, 3, "");
Text humText(NULL, ILI9341_ORANGE, 2, "");
int weatherCode;
bool isDay;

#if ESP8266
ESP8266WebServer server(80);
#else
WebServer server(80);
#endif
WiFiUDP wifiUdp;
NTP ntp(wifiUdp);

String ntpUrl("pool.ntp.org");
int buzzerActive;

void handleRoot() {
  String message;
  message += "<!DOCTYPE html>\n<html>\n<head>\n<meta charset=\"UTF-8\">\n<title>TFT Monitor Configuration</title>\n";
  message += "</head>\n<body>\n";
  message += "<h1>TFT Monitor Configuration</h1>\n";
  message += "<br>Please enter here:<br>";
  message += "- the URL of your DOMOTICZ server<br>";
  message += "- the URL for the Saint of the day php service<br>";
  message += "- the URL of the NTP server<br>";
  message += "- the weather display period in hours<br>";
  message += "- the temperature display period in minutes<br>";
  message += "- the informations display period in minutes<br>";
  message += "Use the \"Reset to Default Values\" button to load the default values<br><br>";
  if (weatherServerError[0] != 0) {
    message += "The last weather server call error was: ";
    message += weatherServerError;
  }
  message += "<form action=\"/form-handle\" method=\"post\">\n<br>";

  message += "<label for=\"domoticz_url\">DOMOTICZ URL: </label>\n<br>";
  message += "<input type=\"text\" id=\"domoticz_url\" name=\"domoticz_url\" value=\"";
  message += domoticzUrl;
  message += "\"size=\"50\"><br><br>\n";

  message += "<label for=\"php_url\">Saint of the Day URL: </label>\n<br>";
  message += "<input type=\"text\" id=\"php_url\" name=\"php_url\" value=\"";
  message += ephemerisUrl;
  message += "\"size=\"50\"><br><br>\n";

  message += "<label for=\"ntp_url\">NTP Server: </label>\n<br>";
  message += "<input type=\"text\" id=\"ntp_url\" name=\"ntp_url\" value=\"";
  message += ntpUrl;
  message += "\"size=\"50\"><br><br>\n";

  message += "<label for=\"weather_city\">Weather for this CITY: </label>\n<br>";
  message += "<input type=\"text\" id=\"weather_city\" name=\"weather_city\" value=\"";
  message += weatherCity;
  message += "\"size=\"50\"><br><br>\n";

  message += "<label for=\"weather_key\">Weather Key: </label>\n<br>";
  message += "<input type=\"text\" id=\"weather_key\" name=\"weather_key\" value=\"";
  message += weatherKey;
  message += "\"size=\"50\"><br><br>\n";

  message += "<label for=\"weather_period\">Weather Display Period: </label>\n<br>";
  message += "<input type=\"number\" min=\"1\" max=\"24\" id=\"weather_period\" name=\"weather_period\" value=\"";
  message += weatherPeriod / (60 * 60000);
  message += "\"size=\"5\"> hours<br><br>\n";

  message += "<label for=\"weather_api_usage\">Actual Wheather API Usage: </label>\n<br>";
  message += "<input type=\"number\" min=\"1\" max=\"1000\" id=\"weather_api_usage\" name=\"weather_api_usage\" value=\"";
  message += weatherApiUsage;
  message += "\"size=\"5\"> calls<br><br>\n";

  message += "<label for=\"temp_period\">Temperature Display Period: </label>\n<br>";
  message += "<input type=\"number\" min=\"1\" max=\"480\" id=\"temp_period\" name=\"temp_period\" value=\"";
  message += tempPeriod / 60000;
  message += "\"size=\"5\"> minutes<br><br>\n";

  message += "<label for=\"info_period\">Informations Display Period (minutes): </label>\n<br>";
  message += "<input type=\"number\" min=\"1\" max=\"480\" id=\"info_period\" name=\"info_period\" value=\"";
  message += infoPeriod / 60000;
  message += "\"size=\"5\"> minutes<br><br>\n";

  for (int i = 0 ; i < NDEVICES ; i++) {
    char name[10];
    message += "<label for=\"info" + String(i + 1) + "\">Device " + String(i + 1) + " (idx, prompt)";
    message += ": </label>\n<br>";
    sprintf(name, "device%d_idx", i+1);
    message += "<input type=\"number\" min=\"1\" max=\"1000\" id=\"" + String(name) + "\" name=\"" + String(name) + "\"";
    message += "\" value=\"";
    message += deviceList[i].idx;
    message += "\" size=\"5\">\n";
    sprintf(name, "device%d_prompt", i+1);
    message += "<input type=\"text\" id=\"" + String(name) + "\" name=\"" + String(name) + "\"";
    message += "\" value=\"";
    message += deviceList[i].prompt;
    message += "\"size=\"10\">\n";
    sprintf(name, "device%d_temp", i+1);
    message += "<input type=\"checkbox\" id=\"" + String(name) + "\" name=\"" + String(name) + "\"";
    message += deviceList[i].temp ? " checked" : "";
    message += "\n";
    message += "<label for \"" + String(name) + "\">Temp</label>\n";
    sprintf(name, "device%d_hum", i+1);
    message += "<input type=\"checkbox\" id=\"" + String(name) + "\" name=\"" + String(name) + "\"";
    message += deviceList[i].hum ? " checked" : "";
    message += "\n";
    message += "<label for \"" + String(name) + "\">Humidity</label>\n";
    sprintf(name, "device%d_batt", i+1);
    message += "<input type=\"checkbox\" id=\"" + String(name) + "\" name=\"" + String(name) + "\"";
    message += deviceList[i].batt ? " checked" : "";
    message += "\n";
    message += "<label for \"" + String(name) + "\">Battery</label>\n";
    message += "<br><br>\n";
  }
  message += "<br><input type=\"reset\" value=\"Reset to Default Values\">";
  message += " <input type=\"submit\" value=\"Send\">\n</form>\n";
  message += "</body>\n</html>\n";
  server.send(200, "text/html", message);
}

void handleFormData() {
  String message;

  for (int cnt = 0 ; cnt < server.args() ; cnt++) {
    log_print(cnt); log_print(" "); log_print(server.argName(cnt));  log_print("=");  log_println(server.arg(cnt));
  }
  domoticzUrl = server.arg("domoticz_url");
  ephemerisUrl = server.arg("php_url");
  ntpUrl = server.arg("ntp_url");
  weatherCity = server.arg("weather_city");
  weatherKey = server.arg("weather_key");
  weatherPeriod = atoi(server.arg("weather_period").c_str()) * 60 * 60000;
  weatherApiUsage = atoi(server.arg("weather_api_usage").c_str());
  tempPeriod = atoi(server.arg("temp_period").c_str()) * 60000;
  infoPeriod = atoi(server.arg("info_period").c_str()) * 60000;
  for (int i = 0 ; i < NDEVICES ; i++) {
    char name[10];
    sprintf(name, "device%d_idx", i+1);
    deviceList[i].idx = atoi(server.arg(name).c_str());
    sprintf(name, "device%d_prompt", i+1);
    strcpy(deviceList[i].prompt, server.arg(name).c_str());
    sprintf(name, "device%d_temp", i+1);
    deviceList[i].temp = server.hasArg(name) ? true : false;
    sprintf(name, "device%d_hum", i+1);
    deviceList[i].hum = server.hasArg(name) ? true : false;
    sprintf(name, "device%d_batt", i+1);
    deviceList[i].batt = server.hasArg(name) ? true : false;
  }
  saveConfiguration();
  displayInfo();
  message += "<!DOCTYPE html>\n<html>\n<head>\n<meta charset=\"UTF-8\">\n<title>TFT Monitor Configuration</title>\n";
  message += "</head>\n<body>\n";
  message += "<h1>TFT Monitor Configuration</h1>\n";
  message += "Configuration has been saved<br><br>";
  message += "domoticz: " + domoticzUrl + "<br>";
  message += "php_url: " + ephemerisUrl + "<br>";
  message += "ntp_url: " + ntpUrl + "<br>";
  message += "wheather_city: ";
  message += weatherCity;
  message += "<br>";
  message += "wheather_key: ";
  message += weatherKey;
  message += "<br>";
  message += "wheather_period: ";
  message += weatherPeriod;
  message += "<br>";
  message += "wheather_api_usage: ";
  message += weatherApiUsage;
  message += "<br>";
  message += "temp_period: ";
  message += tempPeriod;
  message += "<br>";
  message += "info_period: ";
  message += infoPeriod;
  message += "<br>";
  message += "</body>\n</html>\n";
  server.send(404, "text/html", message);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void beep(int n)
{
  for (int i = 0 ; i < n ; i++) {
    digitalWrite(BUZ_PIN, HIGH);
    delay(200);
    digitalWrite(BUZ_PIN, LOW);
    delay(200);
  }
}

void displayTime(void)
{
  int16_t x = 0, y = 10, x1, y1, w, h;
  char buf[21]; // temp buffer for max 20 char display

  strcpy(dayText.buf, ntp.formattedTime("%A"));
  strcpy(dateText.buf, ntp.formattedTime("%d-%m-%Y"));
  y += dayText.printLeft(y);
  y += 5;
  y += dateText.printLeft(y);
  y += 5;
  y += saintText.printLeft(y);
  y += 5;
  y += weatherText.printLeft(y);
  y += 30;
  strcpy(timeText.buf, ntp.formattedTime("%H:%M"));
  timeText.printCentered(y);
}

void displayWeather(void)
{
  static int currentWeatherCode;
  static bool currentDay;

  if (currentWeatherCode != weatherCode || currentDay != isDay) {
    drawBitmap(weatherCode, isDay, tft.width() - 64, 0);
    currentWeatherCode = weatherCode;
    currentDay = isDay;
  }
}

void displayTemp(void)
{
  float temperature;
  float humidity;

  getTemperatureHumidity(&temperature, &humidity);
  int16_t y = 145;
  log_printf("Temp/Hum %f %f\n", temperature, humidity);
  dtostrf(temperature, 5, 2, tempText.buf);
  strcat(tempText.buf, "\xf7");
  tempText.printLeft(y);
  dtostrf(humidity, 5, 2, humText.buf);
  strcat(humText.buf, "%");
  y += tempText.getHeight() - humText.getHeight() - 1;
  humText.printRight(y);
}

void displayInfo(void)
{
  int16_t y = 188;
#define SZ    8
  float temp[NDEVICES];
  unsigned hum[NDEVICES];
  unsigned batt[NDEVICES];
  Text *lines[NDEVICES] = {&infoText1, &infoText2, &infoText3, &infoText4};

  for (int i = 0 ; i < NDEVICES ; i++) {
    float *t = deviceList[i].temp ? &temp[i] : 0;
    unsigned *h = deviceList[i].hum ? &hum[i] : 0;
    unsigned *b = deviceList[i].batt ? &batt[i] : 0;
    getSensorInfo(deviceList[i].idx, t, h, b);
    sprintf(lines[i]->buf, "%-7s", deviceList[i].prompt);
    if (deviceList[i].temp) {
      sprintf(lines[i]->buf+strlen(lines[i]->buf), " %2.2f\xf7", temp[i]);
    }
    if (deviceList[i].hum) {
      sprintf(lines[i]->buf+strlen(lines[i]->buf), " %d%%", hum[i]);
    }
  }
  log_println(F("Display informations"));
  y += infoText1.printLeft(y);
  y += 10;
  y += infoText2.printLeft(y);
  y += 10;
  y += infoText3.printLeft(y);
  y += 10;
  y += infoText4.printLeft(y);
  y += 15;

  infoText5.setBackColor(ILI9341_RED);
  infoText5.setColor(ILI9341_YELLOW);
  buzzerActive = false;
  log_println("checking temp and batteries");
  for (int i = 0 ; i < NDEVICES ; i++) {
    if (!strcmp(deviceList[i].prompt, "Freezer")) {
      log_printf("Freezer: %f\n", temp[i]);
      if (temp[i] > -18) {
        strncpy(infoText5.buf, "Freezer Temp:High", sizeof(infoText5.buf));
        buzzerActive = true;
      }
    }
    if (deviceList[i].batt) {
      log_printf("%s: %u%%\n", deviceList[i].prompt, batt[i]);
      if (batt[i] < 20) {
        sprintf(infoText5.buf, "%s Batt", deviceList[i].prompt);
        buzzerActive = true;
      }
    }
  }
  if (!buzzerActive) {
    infoText5.setBackColor(ILI9341_LIGHTGREY);
    infoText5.setColor(ILI9341_BLUE);
    strncpy(infoText5.buf, "Everything is Fine", sizeof(infoText5.buf));
  }
  infoText5.printCentered(0);
}

void setup(void)
{
  char hostName[12];
  uint8_t mac[6];

  Serial.begin(115200);
  pinMode(BUZ_PIN, OUTPUT);
  digitalWrite(BUZ_PIN, LOW);
  SPI.setFrequency(ESP_SPI_FREQ);
  tft.begin();
  tft.setRotation(2);
  Text::setMargin(10);
  tft.fillScreen(ILI9341_BLACK);

  WiFi.mode(WIFI_STA);
  //  WiFi.config(INADDR_NONE, INADDR_NONE, INADDR_NONE);
  //  WiFi.macAddress(mac);
  //  sprintf(hostName, "Esp32%x%x%x", mac[3], mac[4], mac[5]);
  //  log_printf("setting hostname %s: %d\n", hostName, WiFi.hostname(hostName));
  log_print("\nConnecting to "); log_println(ssid);
  WiFi.begin(ssid, password);
  log_println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    log_print(".");
  }
  beep(1);
  log_print("Connected to "); log_println(ssid);
  log_print("IP address: ");
  log_println(WiFi.localIP());
  if (!SPIFFS.begin()) {
    log_println("An Error has occurred while mounting SPIFFS");
  }
  // Europe/Paris : last sunday of october at 3:00 : 1H offset
  ntp.ruleSTD("CET", Last, Sun, Oct, 3, 60);
  // Europe/Paris : last sunday of march at 2:00 : 2H offset
  ntp.ruleDST("CEST", Last, Sun, Mar, 2, 120);
  ntp.begin();
  loadConfiguration();
  tempSensorBegin();

  if (MDNS.begin("esp8266")) {
    log_println("MDNS responder started");
  }
  server.on("/", handleRoot);
  server.on("/form-handle", handleFormData);
  server.onNotFound(handleNotFound);
  server.begin();
  log_println("HTTP server started");
}

void loop(void)
{
  static bool justBooted = true;
  static unsigned long lastTimeTick;
  static unsigned long lastWeatherTick;
  static unsigned long lastTempTick;
  static unsigned long lastInfoTick;
  static unsigned lastWeatherApiUsage;

  server.handleClient();
#if ESP8266
  MDNS.update();
#else
  if (MDNS.begin("esp32")) {
    log_println("MDNS responder started");
  }
#endif
  unsigned long tick = millis();
  if (justBooted || (ntp.hours() == 0 && ntp.minutes() == 0)) {
    getSaintOfTheDay(saintText.buf, sizeof(saintText.buf));
  }
  if (justBooted || tick - lastTimeTick > 1000) {
    displayTime();
    lastTimeTick = tick;
  }
  if (justBooted == 1 || tick - lastWeatherTick > weatherPeriod) {
    log_print("weather month:"); log_println(currentMonth);
    if (currentMonth != ntp.month()) {
      log_print("ntp month:"); log_println(ntp.month());
      log_println("reset calls counter");
      currentMonth = ntp.month();
      weatherApiUsage = 0;
      saveConfiguration();
    }
    isDay = getWeatherInfo(weatherText.buf, sizeof(weatherText.buf), &weatherCode);
    lastWeatherTick = tick;
    if (lastWeatherApiUsage != weatherApiUsage) {
      saveConfiguration();
    }
    else if (weatherServerError[0] != '\0') {
      saveConfiguration();
    }
    displayWeather();
  }
  if (justBooted || tick - lastTempTick > tempPeriod) {
    displayTemp();
    lastTempTick = tick;
  }
  if (justBooted || tick - lastInfoTick > infoPeriod) {
    displayInfo();
    lastInfoTick = tick;
  }
  if (buzzerActive == 1) {
    beep(4);
    buzzerActive = 0;
  }
  justBooted = false;
}
