# DOMOTICZ TFT MODULE

The purpose of this page is to explain step by step the realization of an TFT display based on ESP8266, with temperature and humidity sensor, connected by WIFI to a DOMOTICZ server.

The board uses the following components :

 * an ESP8266 board (NodeMCU ESP12E, WEMOS D1 MINI, etc.)
 * an ILI9341 TFT display
 * a SHT31D, HDC2080 or HTU21D sensor module
 * the board is powered by the USB connector.

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.11.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2020/04/un-afficheur-tft-pour-domoticz-ou.html

